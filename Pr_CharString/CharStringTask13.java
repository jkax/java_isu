import java.util.Scanner;
public class CharStringTask13 {

	public static void main(String []args) {

		/*
		 * Дана строка, состоящая из русских слов и знаков препинания, 
		 * разделенных пробелами (одним или несколькими). 
		 * Найти количество слов, которые начинаются и заканчиваются одной и той же буквой.
		 */
		
		//String str = "Анаграмма - литературный приём, состоящий в перестановке букв или звуков определённого слова (или словосочетания), что в результате даёт другое слово или словосочетание.";
		String str = InputKeyboardString();
	
		String[] arrStr = str.split("[^а-яА-ЯёЁ]");
		int count = 0;
		for (String val : arrStr) {
			if(val.length() > 1 && (Character.toLowerCase(val.charAt(0)) == Character.toLowerCase(val.charAt(val.length()-1))))
				count++;
		}

		System.out.printf("Ответ: %d", count);
	}
	
	public static String InputKeyboardString() {
		System.out.println("Введите строку: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextLine();
	}
} 
