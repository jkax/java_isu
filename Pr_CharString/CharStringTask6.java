import java.util.Scanner;
public class CharStringTask6 {

	public static void main(String []args) {

		/*
		 * Дана строка, изображающая целое положительное число. Вывести сумму цифр этого числа.
		 */
	
		int res = 0;
		
		for (char ch : InputKeyboardChar().toCharArray()) {
			if(Character.isDigit(ch))
				res = res + Character.getNumericValue(ch);
		}

		System.out.printf("Ответ %d", res);
	}
	
	public static String InputKeyboardChar() {
		System.out.println("Введите: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextLine();
	}
} 
