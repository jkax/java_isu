import java.util.Scanner;
public class CharStringTask1 {

	public static void main(String []args) {

		/*
		 * Некоторый символ C, изображающий цифру или букву (латинскую или русскую), введен с клавиатуры. 
		 * Если C изображает цифру, то вывести строку «digit», если латинскую букву — вывести строку «lat», если русскую — вывести строку «rus».
		 */
	
		char c = InputKeyboardChar();
		String res;
		
		if(Character.isDigit(c))
			res = c + "=digit";
		else if ((int)c >= 65 && (int)c <= 122) 
			res = c + "=lat"; 
		else if((int)c >= 1040 && (int)c <= 1103) 
			res = c + "=rus"; 
		else
			res = "Не определено";
			
		System.out.println(res);
	}
	
	public static char InputKeyboardChar() {
		System.out.println("Введите: ");
		Scanner scan = new Scanner(System.in);
		return scan.next().charAt(0);
	}
} 
