import java.util.Scanner;
public class CharStringTask3 {

	public static void main(String []args) {

		/*
		 * Дана строка. Подсчитать количество содержащихся в ней цифр.
		 */
	
		int res = 0;
		
		for (char ch : InputKeyboardChar().toCharArray()) {
			if(Character.isDigit(ch))
				res++;
		}

		System.out.printf("Ответ %d", res);
	}
	
	public static String InputKeyboardChar() {
		System.out.println("Введите: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextLine();
	}
} 
