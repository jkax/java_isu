import java.util.Scanner;
public class CharStringTask8 {

	public static void main(String []args) {

		/*
		 * Дан символ C и строка S. Удвоить каждое вхождение символа C в строку S.
		 */
	
		char inChar = InputKeyboardChar();
		String res = "";
		for (char ch : InputKeyboardString().toCharArray()) {
			if(inChar == ch)
				res = res + ch + ch;
			else
				res = res + ch;
		}

		System.out.printf("Ответ %s", res);
	}
	
	public static String InputKeyboardString() {
		System.out.println("Введите строку: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextLine();
	}
	
	public static char InputKeyboardChar() {
		System.out.println("Введите символ: ");
		Scanner scan = new Scanner(System.in);
		return scan.next().charAt(0);
	}
} 
