import java.util.Scanner;
public class CharStringTask14 {

	public static void main(String []args) {

		/*
		 * Дана строка, состоящая из русских слов и знаков препинания, разделенных пробелами (одним или несколькими). 
		 * Найти длину самого длинного слова.
		 */
		
		String str = InputKeyboardString();
	
		String[] arrStr = str.split("[^а-яА-ЯёЁ]");
		int maxWord = 0;
		for (String val : arrStr) {
			if(val.length() > maxWord) {
				maxWord = val.length();
			}
		}

		System.out.printf("Ответ: %d", maxWord);
	}
	
	public static String InputKeyboardString() {
		System.out.println("Введите строку: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextLine();
	}
} 
