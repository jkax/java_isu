import java.util.Scanner;
public class CharStringTask2 {

	public static void main(String []args) {

		/*
		 * Дана непустая строка S. Вывести строку, содержащую символы строки S, между которыми вставлено по одному пробелу.
		 */
	
		String res = "";
		
		for (char ch : InputKeyboardChar().toCharArray()) {
			if(ch != ' ')
				res = res + ch + " ";
		}

		System.out.printf("Ответ %s", res);
	}
	
	public static String InputKeyboardChar() {
		System.out.println("Введите: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextLine();
	}
} 
