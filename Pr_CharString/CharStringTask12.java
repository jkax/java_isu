import java.util.Scanner;
public class CharStringTask12 {

	public static void main(String []args) {

		/*
		 * Дана строка, содержащая по крайней мере один символ пробела. 
		 * Вывести подстроку, расположенную между первым и последним пробелом исходной строки. 
		 * Если строка содержит только один пробел, то вывести пустую строку. 
		 */
		
		String str = InputKeyboardString();
		String res = "";
		String[] arrStr = str.split(" ");
		int count = arrStr.length;
		
		if (count == 1)
			res = "";
		else {
			for(int i = 1; i < count-1; i++)
				res = res + arrStr[i] + " ";
		}
		
		System.out.println(res);
	}
	
	public static String InputKeyboardString() {
		System.out.println("Введите строку: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextLine();
	}
} 
