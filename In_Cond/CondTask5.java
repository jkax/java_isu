import java.util.Scanner;

public class CondTask5 {

	public static void main(String []args) {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Удвоить трехзначное число, введенное с клавиатуры, если оно содержит в своей записи хотя бы одну 1, и возвести в квадрат в противном случае.");
			System.out.println("Введите число");
			int num = scan.nextInt();
			if (num > 99 && num < 1000) {
				if (Integer.toString(num).contains("1") == true) {
					System.out.printf ("Ответ %d\n", num*2);
				}
				else {
					System.out.printf ("Ответ %d\n", num*num);
				}
			}
		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	}
} 
