import java.util.Scanner;

public class CondTask8 {

	public static void main(String []args) {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Дано уравнение ах + b = 0. Найти решение этого уравнения или сообщить, что решения не существует.");
			System.out.println("Введите a, b");
			int a = scan.nextInt();
			int b = scan.nextInt();
			
			if (a == 0) {
				System.out.printf("Решений нет");
			}
			else {
				System.out.printf("Ответ: x = %.3f", (double) -b / a);
			}
		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	}
	
} 
