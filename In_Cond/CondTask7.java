import java.util.Scanner;

public class CondTask7 {

	public static void main(String []args) {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Дано трехзначное число, у которого число единиц не превосходит числа сотен. Проверить, является ли оно палиндромом, т. е. числом, которое одинаково читается слева направо и справа налево. Если не является, то вывести ближайшее следующее число-палиндром");
			System.out.println("Введите число");
			int num = scan.nextInt();
			
			if (num > 99 && num < 1000) {
				int temp_last = num % 10;
				int temp_first = num / 10 / 10;
				
			
				if (temp_first >= temp_last) {
					if (temp_first == temp_last) {
						System.out.printf("Ответ: число является палиндромом");
					}
					else {
						System.out.printf("Ответ: %d%d", num / 10, temp_first);
					}
				}
				
			}
		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	}
	
} 
