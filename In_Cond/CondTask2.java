import java.util.Scanner;

public class CondTask2 {

	public static void main(String []args) {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Введите 1-е число");
			int num1 = scan.nextInt();
			System.out.println("Введите 2-е число");
			int num2 = scan.nextInt();
			System.out.println("Введите 3-е число");
			int num3 = scan.nextInt();
			int max = num1;
			if (num2 > max || num3 > max) {
				max = num2 > num3 ? num2 : num3;
			}
			System.out.printf ("Наибольшее число %d\n", max);

		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	
	}
} 
