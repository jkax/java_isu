import java.util.Scanner;

public class CondTask9 {

	public static void main(String []args) {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Составить программу для определения вида четырехугольника, у которого две противоположные стороны параллельны, а две другие — равны. Определить по двум прилежащим сторонам а и b и углу х между ними (в градусах), к какому из четырех видов относится четырехугольник: квадрат, ромб, равнобедренная трапеция, прямоугольник.");
			System.out.println("Введите a, b");
			int a = scan.nextInt();
			int b = scan.nextInt();
			System.out.println("Введите угол");
			int corner = scan.nextInt();
			
			if (a > 0 && b > 0 && corner < 180) {
				
				if (a == b && corner == 90) {
					System.out.printf("Ответ: квадрат");
				}
				else if (a != b && corner == 90) {
					System.out.printf("Ответ: прямоугольник");
				}
				else if (a == b && corner != 90) {
					System.out.printf("Ответ: ромб");
				}
				else {
					// really?
					System.out.printf("Ответ: трапеция");
				}
			}
			else {
				System.out.printf("Err");
			}
		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	}
	
} 
