import java.util.Scanner;
public class RecurTask0 {

	public static void main(String []args) {

		/*
		 * Написать рекурсивный метод writeSquares, который принимает целочисленный параметр n 
		 * и печатает первые n квадратов, разделенных запятой, причем сначала идут квадраты нечетных чисел в порядке убывания, 
		 * а затем – квадраты четных чисел в порядке возрастания. 
		 */
		
		writeSquares(InputKeyboardInt());
		
	}
	
	public static int InputKeyboardInt() {
		System.out.println("Введите: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextInt();
	}
	
	public static void writeSquares(int n) {
		System.out.println(calcSquares(n));
	}
	
	public static String calcSquares(int n) {
		if(n > 1) {
			if(n % 2 == 0) 
				return calcSquares(n-1) + ",  " + n*n;
			else	
				return n*n + ", " + calcSquares(n-1);
		}
		else if (n == 1)
			return n + "";
		
		else
			return "";
				
	}
} 
