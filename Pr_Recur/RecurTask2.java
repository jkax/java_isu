import java.util.Scanner;
public class RecurTask2 {

	public static void main(String []args) {

		/*
		 *Написать рекурсивный метод power(x, n), возвращающий значение вещественного типа, находящий значение n-й степени числа x по формулам:
		 * x0 = 1,
		 * xn = (xn/2)2 при четных n > 0,
		 * xn = x·xn-1 при нечетных n > 0,
		 * xn = 1/x-n при n < 0
		 * x != 0 — вещественное число, n — целое; в формуле для четных n должна использоваться операция целочисленного деления.
		 * С помощью этого метода найти значения xn для данного x при пяти различных значениях n. 
		 */
	
	
	for(int i = 0; i < 6; i++)
		System.out.println(power(InputKeyboardInt(), InputKeyboardInt()));
	
	}
	public static int InputKeyboardInt() {
		System.out.println("Введите: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextInt();
	}
	
	public static float power(int x, int n) {
		if (n == 0) return 1;
		if (n == 1) return (float) x;
		if (n == 2) return (float) x * x;
		if (n % 2 == 0 && n > 0) return power(x, n / 2) * power(x, n / 2);
		if (n % 2 != 0 && n > 0) return x * (power(x, n - 1));
		return 1 / power(x, -n);
	}
} 
