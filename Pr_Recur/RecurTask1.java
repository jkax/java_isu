import java.util.Scanner;
public class RecurTask1 {

	public static void main(String []args) {

		/*
		 * Написать рекурсивный метод fact2(N), возвращающий значение типа long, вычисляющий значение двойного факториала
		 * N!! = N·(N–2)·(N–4)·...
		 * (N > 0 — параметр целого типа; последний сомножитель в произведении равен 2, если N — четное число, и 1, если N — нечетное).
		 * С помощью этого метода вычислить двойные факториалы пяти данных чисел: 0, 5, 8, 10, 20, а также числа, введенного пользователем с клавиатуры. 
		 */
	
	int[] arr = {0, 5, 8, 10, 20};
	for(int val : arr)
		System.out.println("Ответ: " + fact2(val));
	
	System.out.println("Ответ: " + fact2(InputKeyboardInt()));
	
	}
	public static int InputKeyboardInt() {
		System.out.println("Введите: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextInt();
	}
	
	public static long fact2(int n) {
		if(n > 0) {
			if ((n - 2 == 0) || (n - 2 == -1))
				return Long.valueOf(n);
			else
				return Long.valueOf(n*fact2(n-2));
			}
		
		else return 0;
		/*
		if(n > 0) 
			return Long.valueOf(n*fact2(n-2));
		else return 1;
			* */
	}
} 
