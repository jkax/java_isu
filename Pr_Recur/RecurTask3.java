import java.util.Scanner;
import java.util.Arrays;
public class RecurTask3 {

	public static void main(String []args) {

		/*
		 * Написать рекурсивный метод int getSum(int[] a), который на вход принимает массив целых чисел и возвращает сумму элементов массива.
		 * Продемонстрируйте работу метода на нескольких примерах массивов
		 */
		int[] arr1 = {10, 10, 10, 10, 10};
		System.out.println(getSum(arr1));
	
		int[] arr2 = {50, 50};
		System.out.println(getSum(arr2));
		
		int[] arr3 = {-1};
		System.out.println(getSum(arr3));
	}
	
	public static int getSum(int[] arr) {
		if(arr.length >= 2)
			return arr[arr.length-1] + getSum(Arrays.copyOf(arr, arr.length - 1));
		return arr[0];
	}
	
	
} 
