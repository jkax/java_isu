import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;

public class RecurTask4 {
	
	public static int countFiles = 0;
	public static int avgSize = 0;
	public static ArrayList<File> lstFiles = new ArrayList<File>();
	
	public static void main(String []args) {

		/*
		 * Напишите рекурсивный метод, который в качестве параметра принимает строку с описанием пути к некоторой папке на диске, 
		 * рекурсивно обходит вложенные папки и файлы и выполняет задание, указанное в варианте. Продемонстрируйте работу метода.
		 * 
		 * Перед началом работы программа должна проверить указанный путь на корректность. 
		 * Если такой папки на диске не существует, то нужно вывести соответствующее сообщение.
		 * 
		 * Вариант:  Подсчитать средний размер файлов, расположенных в указанной папке и вложенных папках. 
		 * Дополнительно вывести абсолютный путь к файлу, размер которого по абсолютной величине наиболее близок к среднему. 
		 */
	
		String dir = "RecurTask3_test";
		File parentDir = new File(dir);
		if(parentDir.isDirectory()) {
			
			getAvgSum(parentDir);
											
			float avg = (float)avgSize/countFiles;
			File avgFile;
			String resFileName = "";
			float difference = avg;
			for(File val : lstFiles) {
				if( Math.abs(avg - val.length()) < difference) {
					difference = Math.abs(avg - val.length());
					resFileName = val.getAbsoluteFile().toString();
				}
			}
			System.out.println("AvgSize: " + (float)avgSize/countFiles);
			System.out.println(resFileName);

		}
		else {
			System.out.println("Каталог не сущуествует");
		}
	
	}
		
	public static void getAvgSum (File parentPath) {
		ArrayList<String> people = new ArrayList<String>();
		for (File val : parentPath.listFiles()) {
				
			if(val.isDirectory()) 
				getAvgSum(val);

			else {
				countFiles++;
				avgSize += val.length();
				lstFiles.add(val);
			}
		}

	}
} 
