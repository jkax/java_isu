import java.awt.Color;

abstract public class Shape {
	private Color color, backgroundColor;

    public abstract double getPerimetr();
    
    public abstract double getArea();
    
    public abstract void draw();
    
    public void setColor(Color c) { this.color = c; }
    
    public void setBackgroundColor(Color c) { this.backgroundColor = c; }
    
}
