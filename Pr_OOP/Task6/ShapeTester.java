public class ShapeTester{
	
	public static void main (String[] args) {
		
		RegularPolygon regPolygon = new RegularPolygon(8, 4, 0, 0);
		regPolygon.getTopPoints();
		System.out.println(regPolygon);
		System.out.printf("AREA %s\n",regPolygon.getArea());
		
		regPolygon.move(new Point2d(5, 0));
		System.out.printf("MOVE: \n%s", regPolygon);
		regPolygon.draw();
		
		regPolygon.resize(0.5);
		System.out.printf("RESIZE: \n%s", regPolygon);
		regPolygon.setColor(StdDraw.BOOK_RED);
		regPolygon.draw();
			
		regPolygon.turn(100);
		System.out.printf("TURN: \n%s", regPolygon);
		//regPolygon.draw();
			
		System.out.printf("PERIMETR %.2f\n",regPolygon.getPerimetr());
		System.out.printf("WIDTH (2*R) %.2f\n",regPolygon.getWidth());
		System.out.printf("HEIGTH (2*R) %.2f\n",regPolygon.getHeight());
		System.out.printf("CENTER %s\n",regPolygon.getCenter());
		System.out.printf("AREA %s\n",regPolygon.getArea());
		
		RegularPolygon rP1 = new RegularPolygon(8, 4, 0, 0);
		rP1.getTopPoints();
		RegularPolygon rP2 = new RegularPolygon(8, 4, 1, 0);
		rP2.getTopPoints();
		
		System.out.println(rP1.equals(rP2));
		
	}
}

