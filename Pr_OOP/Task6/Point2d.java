//package Task6;

public class Point2d extends Point3d {

    private double x;
    private double y;
    private double min = 0;
    private double max = 10;
   
    public Point2d(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point2d() {
        this.x = min + (int) (Math.random() * max);
        this.y = min + (int) (Math.random() * max);
    }
    
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public void setX(double value) {
        this.x = value;
    }

    public void setY(double value) {
        this.y = value;
    }
    
  
    public boolean equals(Point2d other) {
		return this.x == other.getX() && this.y == other.getY();
	}
	
	public double getDistanceTo(Point2d other) {
		return Math.sqrt( Math.pow(this.x - other.getX(), 2) + Math.pow(this.y - other.getY(), 2));
	}
	
	public String toString() {
		return String.format("(%.1f, %.1f)", this.x, this.y);
	}
	
}

