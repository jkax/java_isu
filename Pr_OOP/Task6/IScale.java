
public interface IScale {
	
	void resize(double value);
	
	double getWidth();
	
	double getHeight();
}
