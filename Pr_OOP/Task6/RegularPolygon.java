/*
 * Подкласс класса Shape — класс RegularPolygon (правильный многоугольник задается количеством вершин, центром и радиусом).
 * Класс должен реализовывать интерфейсы IMove, IScale, ISwing.
 * Для реализации поворота фигуры на заданный угол можно использовать матрицу поворота в двумерном пространстве.
 */

import java.awt.Color;
import java.util.ArrayList;

public class RegularPolygon extends Shape implements IScale, IMove, ISwing {

	private Color color, backgroundColor;
	
	private int numTop;
	private double radius;
	private double perimetr;
	private double side;
	Point2d center;
	private Point2d[] points;
	

	RegularPolygon(int numTop, double radius, double x, double y) {
		this.numTop = numTop;
		this.radius = radius;
		this.center = new Point2d(x, y);
		this.points = new Point2d[numTop];
	}
	
	RegularPolygon(int numTop, double radius) {
		this.numTop = numTop;
		this.radius = radius;
		this.center = new Point2d();
		this.points = new Point2d[numTop];
	}
	
	RegularPolygon(int numTop, double radius, Point2d center) {
		this.numTop = numTop;
		this.radius = radius;
		this.center = center;
		this.points = new Point2d[numTop];
	}
	
	@Override
	public double getPerimetr() {
		return this.side * this.numTop;
	}
	
	@Override
	public double getArea() { 
		// n/2*R^2*sin(2pi/n)
		return ( this.numTop/2 * Math.pow(this.radius, 2) * Math.sin((2 * Math.PI) / this.numTop) ) ; 
	}
	
	@Override
	public void draw() { 
		StdDraw.setXscale(-1, 1);
		StdDraw.setYscale(-1, 1);
		StdDraw.setPenColor(this.color);
		
		double[] x = new double[this.numTop];
		double[] y = new double[this.numTop];
		for(int i = 0; i < this.numTop; i++) {
			x[i] = this.points[i].getX() / 10;
			y[i] = this.points[i].getY() / 10;
		}
		StdDraw.filledPolygon(x, y);
	}
	
	@Override
	public void setColor(Color c) { this.color = c; }
	
	@Override
	public void setBackgroundColor(Color c) { this.backgroundColor = c; }
	

	@Override
	public void resize(double value) {
		this.radius = this.radius * value;
		getTopPoints();

	}
	@Override
	public double getWidth() { return this.radius * 2; }
	@Override	
	public double getHeight() {
		double a = this.points[0].getDistanceTo( this.points[1] );
		return this.radius * 2;
	}
	
	@Override
	public void move(Point2d direction) {
		for(Point2d p : this.points) {
			p.setX(p.getX() + direction.getX());
			p.setY(p.getY() + direction.getY());
		}
		this.center = direction;
	}
	
	@Override
	public Point2d getCenter() { return this.center; }
	
	@Override
	public void turn(double alpha) {
		double cos = (Math.cos(alpha));
		double sin = (Math.sin(alpha));
		for(Point2d p : this.points) {
			double x = p.getX() * cos -  p.getY() * sin;
			double y = p.getX() * sin + p.getY() * cos;
			p.setX(x);
			p.setY(y);
		}
	}
	
	public void getTopPoints() {
		double temp = (2 * Math.PI / this.numTop);
		double x, y;
		double[] arr = new double[this.numTop];
		
		for (int i = 0; i < this.numTop; ++i) {
			x =  this.center.getX() + this.radius * Math.cos(temp * i);
			y =  this.center.getY() + this.radius * Math.sin(temp * i);
			
			this.points[i] = new Point2d(x, y);
		}
		
		this.side = this.points[0].getDistanceTo( this.points[1] );
		this.perimetr = getPerimetr();
	}
	
	@Override
	public String toString() {
		String per = this.perimetr == 0 ? "не подсчитан" : String.format("%.1f", this.perimetr);
		String side = this.side == 0 ? "не подсчитан" : String.format("%.1f", this.side);
		String ps = "";
		
		if(this.points[0] == null)
			ps += "не заданы";
		else 
			for(Point2d p : this.points) ps += String.format("%s\n", p);
		
		return String.format("Radius %.1f \nPerimetr %s \nCenter %s \nNumTop %d \nSide %s \nКоординаты вершин: \n%s \n\n", 
							this.radius, per, this.center, this.numTop, side, ps); 
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof RegularPolygon)) return false;
        RegularPolygon rp = (RegularPolygon) o;
        return 	this.numTop == rp.numTop
				&& this.radius == rp.radius
				&& this.perimetr == rp.perimetr
				&& this.side == rp.side
				&& this.center.equals(rp.center);
	}
}
