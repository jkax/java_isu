import java.util.Scanner;

public class Tester {

	public static void main(String []args) {
		
		double[] in1 = InputKeyboard();
		double[] in2 = InputKeyboard();
		double[] in3 = InputKeyboard();
		Point3d p1 = new Point3d(in1[0], in1[1], in1[2]);	
		Point3d p2 = new Point3d(in2[0], in2[1], in2[2]);	
		Point3d p3 = new Point3d(in3[0], in3[1], in3[2]);	
		//Point3d p1 = new Point3d(0, 0, 0);
		//Point3d p2 = new Point3d(0, 3, 0);
		//Point3d p3 = new Point3d(0, 0, 4);
		
		System.out.printf("p1: %s \np2: %s \np3: %s\n", p1.toString(), p2.toString(), p3.toString());
		
		System.out.printf("p1 equals p2: %b\n", p1.equals(p2));
		System.out.printf("p2 equals p3: %b\n", p2.equals(p3));
		System.out.printf("p3 equals p1: %b\n", p3.equals(p1));

		double CA = computeArea(p1, p2, p3);
		System.out.println(CA == -1 ? " Не является треугольником" : String.format("Площадь треугольника %.1f", CA));
		
	}
	
	public static boolean isTriangle(Point3d p1, Point3d p2, Point3d p3) {
		return (p1.getDistanceTo(p2) < p1.getDistanceTo(p3) + p2.getDistanceTo(p3)) 
				&& (p1.getDistanceTo(p3) < p1.getDistanceTo(p2) + p2.getDistanceTo(p3)) 
				&& (p2.getDistanceTo(p3) < p1.getDistanceTo(p3) + p1.getDistanceTo(p2));
	}
	
	public static double computeArea(Point3d p1, Point3d p2, Point3d p3) {
		if(isTriangle(p1, p2, p3)) {
			double a = p1.getDistanceTo(p2);
			double b = p1.getDistanceTo(p3);
			double c = p2.getDistanceTo(p3);
			double p = (a + b + c)/2;
			
			System.out.printf("a= %.1f b= %.1f c= %.1f\n", a, b, c);
			
			return Math.sqrt(p * (p - a) * (p - b) * (p - c));	
		}
		else return -1;
	}
	
	public static double[] InputKeyboard() {
		double[] resArr = new double[3];
		try {
			System.out.println("Введите: ");
			Scanner scan = new Scanner(System.in);
			String[] arr = scan.nextLine().split(" ");
			
			for(int i = 0; i < 3; i++)
				resArr[i] = Double.parseDouble(arr[i]);
		}
		catch(java.lang.ArrayIndexOutOfBoundsException e){
			System.out.println("Введите 3 значения");
		}
		return resArr;
	}
}
