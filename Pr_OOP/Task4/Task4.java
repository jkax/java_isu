import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.*;

public class Task4 {
	/*
	 * В отдельном классе создайте массив объектов вашего класса, заполните его данными из файла input.txt 
	 * С клавиатуры задайте критерии выбора данных и выведите соответствующие данные на экран в виде таблицы 
	 * с помощью псевдо-графики (используйте статический метод String.format()), например так (ширина полей таблицы должна зависеть от вашего объекта).
	 * Для вывода шапки таблицы и разделителя напишите статические методы в классе, созданном по варианту. 
	*/

	 
	public static void main(String []args) throws IOException {
		
		File file = new File("input.txt");
		Scanner in = new Scanner(file);
		ArrayList<Patient> lst = new ArrayList<>();
		
		int idLine = 0; int count = 0;
		while (in.hasNext()) {
			if(idLine == 0) 
				count = in.nextInt();
			else {
				int id = in.nextInt();
				in.nextLine();
				String lastName = in.nextLine();
				String firstName = in.nextLine();
				String middleName = in.nextLine();
				String phone = in.nextLine();
				long code = in.nextLong();
				in.nextLine();
				String diagnosis = in.nextLine();

				lst.add(new Patient(id, lastName, firstName, middleName, phone, code, diagnosis));
			}
			idLine++;
		}
		in.close();
		
		Patient resPat = new Patient( lst.toArray(new Patient[lst.size()]) , count);
		
		
		System.out.print(resPat.getTableCaption());
		for(Patient person : resPat.arr) {
			System.out.print(person);
			System.out.print(resPat.getLine());
		}
		System.out.print("\n");
		
		resPat.printPatientsDiagnosis("амилоидоз"); 
		resPat.printPatientsLastName("Никитин");
		resPat.printPatientsCode(5733530361L, 8733530361L);
		resPat.printPatientsSort();
	}
}
	
