import java.util.*;

public class Patient {
	/*
	 * Patient: id, Фамилия, Имя, Отчество, Телефон, Номер медицинской карты, Диагноз.
	 * Создать массив объектов. Вывести:
	 * список всех записей;
	 * список пациентов, имеющих данный диагноз;
	 * список пациентов по заданной фамилии;
	 * список пациентов, номер медицинской карты у которых находится в заданном интервале;
	 * список всех записей, отсортированный по фамилии (для сравнения строк используйте метод String.compareTo() ).
	 */
	private int id;
	private String lastName;
	private String firstName;
	private String middleName;
	private String phone;
	private long code;
	private String diagnosis;
	private int widthId = 2;
	public int widthNames = 9;
	private String line = "+";
	private String tableCaption = "+";

	
	public Patient[] arr;
	
	public Patient(int id, String lastName, String firstName, String middleName, String phone, long code, String diagnosis) {
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.phone = phone;
		this.code = code;
		this.diagnosis = diagnosis;
		this.id = id;
		
		this.widthNames = eqLengthNames(lastName, firstName, middleName);
		this.setLine();
		this.setTableCaption();
	}
	
	public Patient(Patient[] patients, int count){
		int maxWidthId = 0;
		int maxWidthNames = 0;
		
		for(int i = 0; i < count; i++){
			if( eqLengthNames( patients[i].lastName, patients[i].firstName, patients[i].middleName ) > maxWidthNames)
				maxWidthNames = eqLengthNames( patients[i].lastName, patients[i].firstName, patients[i].middleName );
		}
		this.widthNames = maxWidthNames;
		this.arr = patients;
		
		this.setLine();
		this.setTableCaption();
		
		for(int i = 0; i < count; i++) {
			patients[i].widthNames = this.widthNames;
			
		}		
	}

	public int eqLengthNames(String lastname, String firstname, String middlename){
		if(lastname.length() > firstname.length() 
		&& lastname.length() > middlename.length())
			return lastname.length();
		
		else if(firstname.length() > lastname.length() 
			&& firstname.length() > middlename.length())
			return firstname.length();
		
		else return middlename.length();
	}

	public int getId() {
		return this.id;
	}
	
	public String getLastName() {
		return this.lastName;
	}
	
	public String getFirstName() {
		return this.firstName;
	}
	
	public String getMiddleName() {
		return this.middleName;
	}
	
	public String getPhone() {
		return this.phone;
	}
	
	public long getCode() {
		return this.code;
	}
	
	public String getDiagnosis() {
		return this.diagnosis;
	}

	public void setId(int value) {
		this.id = value;
	}
	
	public void setLastName(String value) {
		this.lastName = value;
	}
	
	public void setFirstName(String value) {
		this.firstName = value;
	}
	
	public void setMiddleName(String value) {
		this.middleName = value;
	}
	
	public void setCode(long value) {
		this.code = value;
	}
	
	public void setDiagnosis(String value) {
		this.diagnosis = value;
		
	}
	
	public String setFormatValue(String value, int param){
		if (value.length() == param)
			return value;
		else {
			int diff = param - value.length();
			String res = value;
			for(int i = 0; i < diff; i++)
				res += " ";
			
			return res;
			
		}
	}
	@Override
	public String toString() {
		return String.format("| %s | %s | %s | %s |\n", setFormatValue(this.id + "", this.widthId), 
														setFormatValue(this.lastName, this.widthNames), 
														setFormatValue(this.firstName, this.widthNames),
														setFormatValue(this.middleName, this.widthNames));
	}
	
	public void setTableCaption() {
		this.tableCaption = String.format("%s| %s | %s | %s | %s |\n%s", this.line,
														setFormatValue("id", this.widthId), 
														setFormatValue("lastname", this.widthNames), 
														setFormatValue("firstname", this.widthNames),
														setFormatValue("middlename", this.widthNames), this.line);
	}
	
	public void setLine() {
		for(int i = 0; i < (this.widthId + 5 + (this.widthNames + 2)*3); i++)
			this.line +="-";
		this.line += "+\n";
	}
	
	public String getLine() {
		return this.line;
	}
	
	public String getTableCaption() {
		return this.tableCaption;
	}
	
	public int equals(Patient person) {
		return this.lastName.compareTo(person.lastName);
	}
	
	
	public void printPatientsDiagnosis(String value){
		System.out.print(this.getTableCaption());
		for(Patient person : this.arr) {
			if(person.diagnosis.equalsIgnoreCase(value)) {
				System.out.print(person);
				System.out.print(this.getLine());
			}
		}
		System.out.print("\n");
	}
		
	public void printPatientsLastName(String value){
		System.out.print(this.getTableCaption());
		for(Patient person : this.arr) {
			if(person.lastName.equalsIgnoreCase(value)) {
				System.out.print(person);
				System.out.print(this.getLine());
			}
		}
		System.out.print("\n");
	}
	
	public void printPatientsCode(long value1, long value2 ){
		System.out.print(this.getTableCaption());
		for(Patient person : this.arr) {
			if( value1 < person.code && person.code < value2) {
				System.out.print(person);
				System.out.print(this.getLine());
			}
		}
		System.out.print("\n");
	}
	
	public static Comparator<Patient> LastNameComparator = new Comparator<Patient>() {
		@Override
		public int compare(Patient p1, Patient p2) {
			return p1.getLastName().compareTo(p2.getLastName());
		}
	};
	
	public void printPatientsSort(){
		Arrays.sort(this.arr, LastNameComparator );
		System.out.print(this.getTableCaption());
		for(Patient person : this.arr) {
			System.out.print(person);
			System.out.print(this.getLine());
		}
		System.out.print("\n");
	}
}



