public class Point3d {

    private double x;
    private double y;
    private double z;
    public Point3d[] arr;

    public Point3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3d() {
        this.x = 0.0;
        this.y = 0.0;
        this.z = 0.0;
    }
    
    public Point3d(int n) {
		Point3d[] points = new Point3d[n];
		for(int i = 0; i < n; i++) 
			points[i] =  new Point3d(Math.random(), Math.random(), Math.random());
		this.arr = points;
		
	}
    
     public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }
    
    public void setX(int value) {
        this.x = value;
    }

    public void setY(int value) {
        this.y = value;
    }
    
    public void setZ(int value) {
        this.z = value;
    }
    
    public boolean equals(Point3d other) {
		return this.x == other.getX() && this.y == other.getY() && this.z == other.getZ();
	}
	
	public double getDistanceTo(Point3d other) {
		return Math.sqrt( Math.pow(this.x - other.getX(), 2) + Math.pow((this.y - other.getY()), 2) + Math.pow((this.z - other.getZ()), 2));
	}
	
	public String toString() {
		return String.format("(%.1f, %.1f, %.1f)", this.x, this.y, this.z);
	}
	
}

