import java.util.Scanner;
import java.util.ArrayList;

public class RandomPoints {
	/*
	 * Напишите метод, который принимает в качестве параметра массив объектов типа Point3d, 
	 * ссылку на объект типа Point3d – точка центра некоторой сферы и значение типа double – радуис сферы. 
	 * Метод должен вернуть массив, содержащий только те точки из исходного массива, 
	 * которые находятся внутри указанной сферы или на ее поверхности. 
	 */
	 
	public static void main(String []args) {
		
		Point3d points = new Point3d(InputKeyboardInt());
		double d = InputKeyboardDouble();
			
		int counterD = 0;
		Point3d pointNear1 = points.arr[0]; 
		Point3d pointNear2 = points.arr[0];
		int index1=0; int index2=0;
		double distance = 0.0;
		double distanceMin = 1.0; // единичный куб
		
		for(int i = 0; i < 5; i++) {
			for(int j = 0; j < 5; j++) {
				if(points.arr[i].equals(points.arr[j]) == false) {
					if (i < j) {	
						distance = points.arr[i].getDistanceTo(points.arr[j]);
						if(distance < distanceMin) {
							distanceMin = distance;
							pointNear1 = points.arr[i];
							pointNear2 = points.arr[j];
							index1 = i;
							index2 = j;
						}
						if(distance < d) 
							counterD++;
					}
				}
			}
		}
		
		System.out.printf("Количество пар точек, расстояние между которыми меньше d: %d\n", counterD);
		System.out.printf("Координаты ближайших точек: \n %d %s \n %d %s \n", index1, pointNear1.toString(), index2, pointNear2.toString());
		System.out.printf("Расстояние между ближайшими точками: %f\n\n", distanceMin);
		
		Point3d[] sphere = getPointsSphere(points.arr, points.arr[0], d);
		for(Point3d p : sphere)
			System.out.println(p.toString());

	}
	
	public static Point3d[] getPointsSphere(Point3d[] arrPoints, Point3d center, double radius) {
		ArrayList<Point3d> lst = new ArrayList<>();
			for(int j = 0; j < arrPoints.length; j++) {
				if(center.getDistanceTo(arrPoints[j]) <= radius)
					lst.add(arrPoints[j]);
			}
		return lst.toArray(new Point3d[lst.size()]);
	}
	
	public static int InputKeyboardInt() {
		System.out.println("Введите n: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextInt();
	}
	
	public static double InputKeyboardDouble() {
		System.out.println("Введите d <= 1: ");
		Scanner scan = new Scanner(System.in);
		return scan.nextDouble();
	}
}
	
