/* 
 * Напишите класс ArithProgression, наследник класса Progression, 
 * создающий арифметическую прогрессию путем сложения предыдущего элемента прогрессии и постоянной разности прогрессии d. 
 * 
 * В классе должен присутствовать конструктор по умолчанию, который присваивает первое значение последовательности 2, 
 * и параметрический конструктор, зависящий от первого члена прогрессии и разности прогрессии.
 */

public class ArithProgression extends Progression {
	
	protected int d = 1;
	
	ArithProgression() {
		super.first = 2;
	
	}
	
	ArithProgression(long firstVal, int dVal) {
		super.first = firstVal;
		d = dVal;
	}
	
	protected long nextValue() {
		cur += d;
		return cur;
	}
	
}
