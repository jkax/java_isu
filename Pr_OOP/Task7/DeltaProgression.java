/*
 * Напишите класс DeltaProgression, наследник класса Progression, создающий последовательность, 
 * значение членов которой является абсолютным значением разности двух предыдущих членов последовательности. 
 * 
 * В классе должен присутствовать конструктор по умолчанию, который присваивает первые два значения последовательности 2 и 5, 
 * и параметрический конструктор, присваивающий некие значения первой паре последовательности.
 */
 
public class DeltaProgression extends Progression {
	
	protected long prev;

	DeltaProgression() {
		this(2, 5);
	}
	
	DeltaProgression(long firstVal, long secondVal) {
		first = secondVal;
		prev = firstVal;
	}
	protected String startValue() {
		return prev + " ";
	}
	
	protected long nextValue() {
		long temp = cur;
		cur = Math.abs(cur - prev);
		prev = temp;
		return cur;
	}
	
	public void printProgression(int n) {
		
		System.out.print(startValue());
		System.out.print(firstValue());
		for (int i = 3; i <= n; i++) 
			System.out.print(" " + nextValue());
		System.out.println();
	}
}

