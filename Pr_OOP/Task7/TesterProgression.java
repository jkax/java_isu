
public class TesterProgression {
	
	public static void main (String[] args) {
		
		ArithProgression arPr1 = new ArithProgression();
		ArithProgression arPr2= new ArithProgression(4, 20);
		
		System.out.println("\nArithProgression:");
		arPr1.printProgression(5);
		arPr2.printProgression(7);
		
		DeltaProgression dePr1 = new DeltaProgression();
		DeltaProgression dePr2= new DeltaProgression(10, 5);
		System.out.println("\nDeltaProgression:");
		dePr1.printProgression(9);
		dePr2.printProgression(5);
	}
}

