import java.util.Scanner;

public class LogTask3 {
   /*
    Даны два целых числа: A, B. Проверить истинность высказывания: «Ровно одно из чисел A и B нечетное».

	Пример работы программы:
	Введите число A: 4
	Введите число B: 9

	Ответ: true
	Введите число A: 9
	Введите число B: 4

	Ответ: true
	Введите число A: 3
	Введите число B: 9

	Ответ: false
	*/
	public static void main(String []args) {
		try {
			System.out.println("Ровно одно из чисел A и B нечетное. \nA = , B = ");
			Scanner scan = new Scanner(System.in);
			int A = scan.nextInt();
			int B = scan.nextInt();
			System.out.printf ("A = %d, B = %d \n", A, B);
			System.out.println(A % 2 == 0 ^ B % 2 == 0);
		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	
	}
} 
