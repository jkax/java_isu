import java.util.Scanner;

public class LogTask8 {

	public static void main(String []args) {
		try {
			System.out.println("Даны координаты двух различных полей шахматной доски x1, y1, x2, y2 (целые числа, лежащие в диапазоне 1–8). Проверить истинность высказывания: «Слон за один ход может перейти с одного поля на другое».:");
			Scanner scan = new Scanner(System.in);
			int x1 = scan.nextInt();
			int y1 = scan.nextInt();
			int x2 = scan.nextInt();
			int y2 = scan.nextInt();
			System.out.printf("x1 = %d y1 = %d \nx2 = %d y2 = %d \n", x1, y1, x2, y2);
			boolean check =  (x1 >= 1 & x1 <= 8) & (y1 >= 1 & y1 <= 8) & (x2 >= 1 & x2 <= 8) & (y2 >= 1 & y2 <= 8);
			System.out.println(check && (x1 - x2 == y2 - y1 || x1 - x2 == y1 - y2 || x2 - x1 == y2 - y1 || x2 - x1 == y1 - y2));

		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	
	}
} 
