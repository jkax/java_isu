import java.util.Scanner;

public class LogTask4 {
   /*
    * Проверить истинность высказывания: 
	* «Среди трех данных целых чисел есть хотя бы одна пара взаимно противоположных».
	*/
	public static void main(String []args) {
		try {
			System.out.println("Среди трех данных целых чисел есть хотя бы одна пара взаимно противоположных:");
			Scanner scan = new Scanner(System.in);
			int A = scan.nextInt();
			int B = scan.nextInt();
			int C = scan.nextInt();
			System.out.println(A == B*-1 | A == C*-1 | B == C*-1);
		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	
	}
} 
