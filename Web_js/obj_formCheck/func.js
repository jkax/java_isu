$(document).ready(function(){
	var flag_agreement = 0;
	jVal = {
	
		'countVal' : function() {
			if( $(this).val().length < 8) {
				$(this).css({'border' : '3px solid red'}); 
				return 1;
			}
			else {
				$(this).css({'border' : '3px solid green'});
				jVal.errors = true;
				return 0;
			}
		},
			
		'emptyVal' : function() {
			if( $(this).val().length < 1) {
				$(this).css({'border' : '3px solid red'});
			}
			else {
				$(this).css({'border' : '3px solid green'});
				jVal.errors = true;
			}
		},
		
		'checkEmail' : function() {
			var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
			if( pattern.test(  $(this).val() ) ) {
				$(this).css({'border' : '3px solid green'});
				jVal.errors = true;
			}
			else {
				$(this).css({'border' : '3px solid red'});
			}
		},
		
		'sendIt' : function (){
			if(!jVal.errors) {
				$('#submit').submit();
			}
		},
		
		'agreementUpd' : function (){
			flag_agreement++;
			if(flag_agreement % 2 != 0)
				$('#submit').prop('disabled', false);
			else
				$('#submit').prop('disabled', true);
			

		},
		
		'listUpd' : function (){
			$("#city2").prop('disabled', false);
			 
			$('#city2 option').each(function(){
				if( $(this).attr("class") == $('#city1 option:selected').attr("class") ) 
					$(this).show();
				else
					$(this).hide();
			});
		}	
			
	};
	
	$('#login').change(jVal.countVal);
	$('#password').change(jVal.countVal);
	$('#fname').change(jVal.emptyVal);
	$('#lname').change(jVal.emptyVal);
	$('#mname').change(jVal.emptyVal);
	$('#email').change(jVal.checkEmail);
		
	$('#city1').change(jVal.listUpd);	
	$('#agreement').change(jVal.agreementUpd);	
	

});
