function holiday() {
	var date = new Date();
	var answer = document.getElementById("answer");
	var nowId = document.getElementById("now");
	
	if( date.getDay() == 6 ||  date.getDay() == 7 ) {
		var textAnswer = "Да";
		var nameClass = "yes";
	}
	else {
		var textAnswer = "Нет";
		var nameClass = "no";
	}
		
	answer.textContent = textAnswer;
	document.body.className = nameClass;
	
	var month = ["Янв", "Фев", "Мар", "Апр", "Май", "Июн",
				"Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"][date.getMonth()];
	nowId.textContent = ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"][date.getDay()] + ", "
						+ date.getDate() + " "
						+ month + " "
						+ date.getFullYear() + ", "
						+ date.getHours() + ":"
						+ date.getMinutes() + ":"
						+ date.getSeconds();
}

