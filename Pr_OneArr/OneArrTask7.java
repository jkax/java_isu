import java.util.Scanner;
import java.util.Random;
public class OneArrTask7 {

	public static void main(String []args) {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Дан массив A размера N (элементы массива заполняются случайными целыми числами) и целое число K (0 < K < N). Преобразовать массив, увеличив каждый его элемент на исходное значение элемента AK.");
			System.out.println("Введите N");
			int N = scan.nextInt();
			System.out.println("Введите K");
			int K = scan.nextInt();
			
			Random random = new Random();
			int[] arr = new int[N];
			if (N > 1 && (K > 0 && K < N)) {
				for(int i = 0; i < N; ++i) {
					arr[i] = random.nextInt(100);
					System.out.println(arr[i]);
				}
				
				System.out.printf("\n\n");
				
				for(int i = 0; i < N; ++i) {
					arr[i] = arr[i] + K;
					System.out.println(arr[i]);
				}
								
			}
			else {
				System.out.printf("Err");
			}	
				
		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	}
	
} 
