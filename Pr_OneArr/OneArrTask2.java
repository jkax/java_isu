import java.util.Scanner;
import java.util.Random;

public class OneArrTask2 {

	public static void main(String []args) {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Дан массив A размера N (N – четное число, вводится с клавиатуры, элементы массива заполняются случайными целыми числами). Вывести вначале его элементы с нечетными индексами в порядке возрастания индексов, а затем – элементы с четными индексами в порядке убывания индексов: A1, A3, A5, ..., A4, A2, A0. Условный оператор не использовать.");
			System.out.println("Введите N");
			int N = scan.nextInt();
			
			Random random = new Random();
			int[] arr = new int[N];
			if (N % 2 == 0) {
				for(int i = 0; i < N; ++i) {
					arr[i] = random.nextInt(100);
					System.out.printf("key = %d, val = %d \n", i, arr[i]);
				}
				
				System.out.printf("\n\n");
				for(int i = 1; i < N; i = i+2)
					System.out.printf("key = %d, val = %d \n", i, arr[i]);
				
				
				System.out.printf("\n\n");
				for(int i = N-2; i >= 0; i=i-2) 
					System.out.printf("key = %d, val = %d \n", i, arr[i]);
			}
			else {
				System.out.printf("Err");
			}	
				
		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	}
	
} 
