import java.util.Scanner;

public class OneArrTask5 {

	public static void main(String []args) {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Дан массив A размера N (элементы массива заполняются числами, введенными с клавиатуры). Найти максимальный элемент из его элементов с нечетными индексами: A1, A3, A5, ...");
			System.out.println("Введите N");
			int N = scan.nextInt();
			
			int[] arr = new int[N];
			if (N > 1) {
				for(int i = 0; i < N; ++i) 
					arr[i] = scan.nextInt();
				
				int max = arr[1];
				for(int i = 1; i < N; i = i+2) {
					if (arr[i] > max)
						max = arr[i];
				}
				System.out.printf("\nОтвет: %d", max);
				
			}
			else {
				System.out.printf("Err");
			}	
				
		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	}
	
} 
