import java.util.Scanner;

public class OneArrTask1 {

	public static void main(String []args) {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Дано целое число N (> 2). Сформировать и вывести целочисленный массив размера N, содержащий N первых элементов последовательности чисел Фибоначчи FK: F1 = 1, F2 = 1, FK = FK-2 + FK-1, K = 3, 4, ...");
			System.out.println("Введите N");
			int N = scan.nextInt();
			
			if (N > 2) {
				int[] fib = new int[N];
				fib[0] = 1; //0
				fib[1] = 1;
				for(int i = 2; i < N; ++i) {
					fib[i] =  fib[i - 1] + fib[i - 2];
				}
				
				for(int i = 0; i < N; ++i) {
					System.out.println(fib[i]);
				}
			}
			else {
				System.out.printf("Err");
			}
			
		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	}
	
} 
