import java.util.Scanner;
import java.util.Random;
public class OneArrTask8 {

	public static void main(String []args) {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Дан массив A размера N (элементы массива заполняются случайными целыми числами) и целые числа K и L (0 <= K < L < N). Переставить в обратном порядке элементы массива, расположенные между элементами AK и AL, не включая эти элементы.");
			System.out.println("Введите N");
			int N = scan.nextInt();
			System.out.println("Введите K");
			int K = scan.nextInt();
			System.out.println("Введите L");
			int L = scan.nextInt();
			
			Random random = new Random();
			int[] arr = new int[N];
			
			if (N > 1 && (K >= 0 && K < L) && L < N) {
				for(int i = 0; i < N; ++i) {
					arr[i] = random.nextInt(100);
					//arr[i] = i;
					System.out.println(arr[i]);
				}
				
				int len = L - (K + 1);
				for(int i = 1; i <= len/2; ++i) {
					int temp = arr[K + i];
					arr[K + i] = arr[L-i];
					arr[L-i] = temp;
				}
							
				System.out.println("\n\n");
				for(int i = 0; i < N; ++i)
					System.out.println(arr[i]);				
			}
			else {
				System.out.printf("Err");
			}	
				
		}
		catch (java.util.InputMismatchException e) {
			System.out.println("Введите целое число");
		}
	}
	
} 
