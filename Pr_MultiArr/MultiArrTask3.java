public class MultiArrTask3 {

	public static void main(String []args) {

		/*
		 * Массив a[n][n] разбивается на четыре четверти, ограниченные главной и побочной диагоналями: верхнюю, нижнюю, левую и правую. 
		 * Напишите метод, который вычисляет сумму элементов левой четверти (без учета элементов, расположенных на диагоналях).
		 */
	
		MultiArrTask1 ObjArr = new MultiArrTask1();
		int N = 8; 
		//int[][] arr = ObjArr.createArrRandom(N, N, -10, 10);
		int[][] arr = ObjArr.createArrFile("arr3.txt");
		ObjArr.printArr(arr);
		
		System.out.printf("Res = %d", sumLeftDiagonsl(arr));
		
	}
	
	public static int sumLeftDiagonsl (int[][] arr) {
		int res = 0;
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < (arr.length/2 - 1); j++) {
				if( i > j) {
					if( i + j < arr.length - 1) {
						res += arr[i][j];
					}
				}
			}
		}
		return res;
	}
} 
