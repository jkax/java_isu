public class MultiArrTask2 {

	public static void main(String []args) {

		/*
		 * Дан массив размерности M на N. Найти последний минимальный элемент двумерного массива и его индексы.
		 */
	
		MultiArrTask1 ObjArr = new MultiArrTask1();
		int rows = 5; int cols = 4;
		int[][] arr = ObjArr.createArrRandom(rows, cols, 0, 10);
		ObjArr.printArr(arr);
		
		int min = arr[0][0];
		int ind_i = 0; int ind_j = 0;
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < cols; j++) {
				if(arr[i][j] <= min) {
					min = arr[i][j];
					ind_i = i;
					ind_j = j;
				}
			}
		}
		
		System.out.printf("min = %d \ni = %d \nj = %d", min, ind_i, ind_j);
		
	}
	
	
} 
