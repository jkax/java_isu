import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
import java.io.*;
import java.util.regex.*;
import java.util.ArrayList;

public class MultiArrTask1 {

	public static void main(String []args) {

		/*
		 * Метод вывода двумерного массива на экран в виде таблицы (используйте форматный вывод). Данному методу в качестве аргумента передается двумерный массив.
		 * Метод для создания и заполнения целочисленного двумерного массива случайными числами, в качестве аргументов передаются количество строк и столбцов массива, минимальное и максимальное значение диапазона случайных чисел. Метод должен возвратить созданный массив.
		 * Метод для создания и заполнения целочисленного двумерного массива значениями, введенными с клавиатуры. Метод должен сначала запросить с клавиатуры количество строк и столбцов массива, а затем и сами элементы. Метод должен возвратить созданный массив
		 * Метод для создания и заполнения целочисленного массива из файла, в качестве аргумента передается имя файла в виде строки (String). В файле сначала записаны два целых числа – число строк и столбцов, а затем сами значения. Метод должен возвратить созданный массив
		 */
		printArr(createArrRandom(3,3, -10, 10));
		//printArr(createArrInputKeyboard());
		printArr(createArrFile("arr.txt"));
		
	}
	
	public static int[][] createArrRandom (int rows, int columns, int min, int max) {

		int[][] arr = new int[rows][columns];
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < columns; j++) {
				arr[i][j] = ThreadLocalRandom.current().nextInt(min, max + 1);
			}
		}
		return arr;
	}
	
	
	public static int[][] createArrInputKeyboard() {
	
		Scanner scan = new Scanner(System.in);
		System.out.println("Rows: ");
		int rows = scan.nextInt();
		System.out.println("Columns: ");
		int columns = scan.nextInt();
		int[][] arr = new int[rows][columns];
		
		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < columns; j++) {
				arr[i][j] = scan.nextInt();
			}
		}
		return arr;	
	}
	
	
	public static int[][] createArrFile(String filename) {
		
		
		String str = openFile(filename);
		str = str.replaceAll("[\n]+", " ");
		str = str.replaceAll("[ ]+", " ");
		int key = 0;
		int rows=0; int cols=0;
		//ArrayList temp = new ArrayList();
		ArrayList<Integer> temp = new ArrayList<Integer>();
		
		for (String retval : str.split(" ")) {
			if(key == 0)
				rows = Integer.parseInt(retval);
			else if (key == 1)
				cols = Integer.parseInt(retval);
			else
				temp.add(Integer.parseInt(retval));
			key++;
        }
		int k_i = 0; int k_j = -1;
		int[][] arr = new int[rows][cols];
		
		for (int val : temp) {
			k_j++;
			if (k_i < rows && k_j < cols) {
				arr[k_i][k_j] = val;
			}
			else {
				k_j = 0;
				k_i++;
				arr[k_i][k_j] = val;
			}
		}
    return arr;
	}
	
	
	public static String openFile(String filename) {
		String str = "";
		try{
			FileInputStream fstream = new FileInputStream(filename);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strLine;
			int i = 0;
			while ((strLine = br.readLine()) != null){
				if (i > 0)
					str = str + " ";
				str = str + strLine;
				i++;
			}
			return str;
		}catch (IOException e){
			System.out.println("Ошибка IO");
			return str;
		}
	}
	
	public static void printArr(int arr[][]) {
		
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < arr[i].length; j++) {
				System.out.printf("%d  ", arr[i][j]);
			}
			System.out.printf("\n");
		}
		System.out.printf("\n");
	}
} 
